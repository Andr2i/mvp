package com.andr2i.mvpcalculator;

import com.andr2i.mvpcalculator.view.CalculatorView;
import com.andr2i.mvpcalculator.view.IMainView;

public class Main {

    public static void main(String[] args) {

        IMainView view = new CalculatorView();
        view.start();

    }


}
