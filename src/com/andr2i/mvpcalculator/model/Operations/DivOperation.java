package com.andr2i.mvpcalculator.model.Operations;

public class DivOperation implements IBinaryOperation {

    @Override
    public double calculate(double op1, double op2) {
         return op1 / op2;
    }

    @Override
    public String getName() {
        return "/";
    }
}