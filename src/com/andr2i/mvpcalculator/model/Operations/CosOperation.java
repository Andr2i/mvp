package com.andr2i.mvpcalculator.model.Operations;

public class CosOperation implements IUnaryOperation {

    @Override
    public double calculate(double op1) {
        return Math.cos(op1);
    }

    @Override
    public String getName() {
        return "cos";
    }
}
