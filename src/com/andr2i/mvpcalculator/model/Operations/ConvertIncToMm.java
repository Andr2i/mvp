package com.andr2i.mvpcalculator.model.Operations;

public class ConvertIncToMm implements IUnaryOperation {

    @Override
    public double calculate(double op1) {
         return op1  * 25.4;
    }

    @Override
    public String getName() {
        return "InchToMm";
    }
}