package com.andr2i.mvpcalculator.model.Operations;

public class SinOperation implements IUnaryOperation {

    @Override
    public double calculate(double op1) {
        return Math.sin(op1);
    }

    @Override
    public String getName() {
        return "sin";
    }
}
