package com.andr2i.mvpcalculator.model.Operations;

public interface IOperation {
    String getName();
}
