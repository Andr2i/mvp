package com.andr2i.mvpcalculator.model.Operations;

public interface IUnaryOperation  extends IOperation {
    double calculate(double op1);
}
