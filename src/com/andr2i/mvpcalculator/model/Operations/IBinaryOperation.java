package com.andr2i.mvpcalculator.model.Operations;

public interface IBinaryOperation extends IOperation {
    double calculate(double op1, double op2);
}
