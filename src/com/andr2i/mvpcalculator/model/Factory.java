package com.andr2i.mvpcalculator.model;

public class Factory {

    public static <T> T getInstance(Class<T> type){
        try {
            return type.newInstance();
        } catch (Exception e) {
            return null;
        }

    }
}