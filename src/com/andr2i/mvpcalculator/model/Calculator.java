package com.andr2i.mvpcalculator.model;

import com.andr2i.mvpcalculator.model.Operations.*;

import java.util.HashMap;

public class Calculator implements ICalcModel {

    private static Calculator instance;
    private HashMap<String, Class<?>> operations = new HashMap<String, Class<?>>();

    private Calculator() {
        initOperations();
    }

    private void initOperations() {

        operations.put("+", AddOperation.class);
        operations.put("-", SubOperation.class);
        operations.put("*", MultOperation.class);
        operations.put("/", DivOperation.class);
        operations.put("Sin", SinOperation.class);
        operations.put("Cos", CosOperation.class);
        operations.put("IncToMm", ConvertIncToMm.class);

    }

    public String getOperationsInformation()
    {
        StringBuilder sb = new StringBuilder();
        for ( String key : operations.keySet() ) {
            sb.append("\"");
            sb.append(key);
            sb.append("\"");
            sb.append(";");
        }
        return sb.toString();
    }

    public static synchronized Calculator getInstance() {
        if (instance == null) {
            instance = new Calculator();
        }
        return instance;
    }

    public Object createOperation(String operation) {

        Class<?> type = operations.get(operation);
        Object ob = null;
        ob = Factory.getInstance(type);

        return ob;
    }

    @Override
    public int getOperandCount(String operator) {
        int res = 0;
        Class<?> type = operations.get(operator);
        if (type != null) {
            if (IUnaryOperation.class.isAssignableFrom(type)) {
                res = 1;
            } else if (IBinaryOperation.class.isAssignableFrom(type)) {
                res = 2;
            }
        }
        return res;
    }

    @Override
    public double calcOperation(double op1, double op2, String operator) {
        IBinaryOperation ob = (IBinaryOperation) createOperation(operator);
        return ob.calculate(op1, op2);
    }

    @Override
    public double calcOperation(double op1, String operator) {
        IUnaryOperation ob = (IUnaryOperation) createOperation(operator);
        return ob.calculate(op1);
    }


}
