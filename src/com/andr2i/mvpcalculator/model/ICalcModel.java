package com.andr2i.mvpcalculator.model;

public interface ICalcModel {
    int getOperandCount(String operator);

    double calcOperation(double op1, String operator);

    double calcOperation(double op1, double op2, String operator);

    String getOperationsInformation();
}