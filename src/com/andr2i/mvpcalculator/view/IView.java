package com.andr2i.mvpcalculator.view;

public interface IView {

    void showText(String value);

    double getOperand();

    void subscribe(IComputationListener listener);

    interface IComputationListener {
        void onCalculate(String operation);
    }
}
