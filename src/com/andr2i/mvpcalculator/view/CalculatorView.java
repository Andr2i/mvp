package com.andr2i.mvpcalculator.view;

import com.andr2i.mvpcalculator.presenter.CalcPresenter;

import java.util.Locale;
import java.util.Scanner;

public class CalculatorView implements IView,IMainView {


    private CalcPresenter presenter;
    private IComputationListener listener;

    public CalculatorView() {
        presenter = new CalcPresenter(this);
    }


    public void start() {
        Scanner scanner = new Scanner(System.in);
        String choice = null;

        System.out.println(String.format("Cписок операций : %s", presenter.getOperationsInformation()));

        while (!"end".equals(choice)) {
            if (choice != null)
                listener.onCalculate(choice);

            System.out.println("Выберите операцию. Для выхода введите \"end\".");
            choice = scanner.nextLine();
        }
        scanner.close();
    }

    @Override
    public void showText(String value) {
        System.out.println(value);
    }

    @Override
    public double getOperand() {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);
        System.out.println("Введите число:");
        double num;
        if (scanner.hasNextDouble()) {
            num = scanner.nextDouble();
        } else {
            System.out.println("Вы допустили ошибку при вводе числа. Попробуйте еще раз.");
            scanner.next();
            num = getOperand();
        }
        return num;
    }

    @Override
    public void subscribe(IComputationListener listener) {
        this.listener = listener;
    }
}