package com.andr2i.mvpcalculator.presenter;

import com.andr2i.mvpcalculator.model.*;
import com.andr2i.mvpcalculator.view.IView;


public class CalcPresenter implements IView.IComputationListener {

    private ICalcModel model;
    private IView gui;

    public CalcPresenter(IView gui) {
        this.model = Calculator.getInstance();

        this.gui = gui;
        gui.subscribe(this);
    }


    public String getOperationsInformation()
    {
       return model.getOperationsInformation();
    }

    @Override
    public void onCalculate(String operationName) {
        double res = 0;
        switch (model.getOperandCount(operationName)) {
            case (1): {
                res = model.calcOperation(gui.getOperand(), operationName);
                break;
            }
            case (2): {
                res = model.calcOperation(gui.getOperand(), gui.getOperand(), operationName);
                break;
            }
            default: {
                gui.showText("Ошибка ввода операции");
                break;
            }
        }
        gui.showText(String.format("\"Результат операции: \" :%s", res));


    }

}
